﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Textifant.Models;

namespace Textifant
{
    public static class Extensions
    {
        public static void Seed(this TextifantContext context)
        {
            // Insert roles
            Role Administrator = new Role { Description = "Administrator" };
            Role User = new Role { Description = "User" };
            context.Roles.Add(Administrator);
            context.Roles.Add(User);

            // Insert companies
            Company Rilegal = new Company { Name = "Rilegal", Street = "Teerlingstraat", ZIP = "9880" , City  = "Aalter",  Country ="Belgium", VAT = "121345658" };
            Company Savaco = new Company { Name = "Savaco", Street = "Beneluxpark 19", ZIP = "8500", City = "Kortrijk", Country = "Belgium", VAT = "121345658" };
            context.Companies.Add(Rilegal);
            context.Companies.Add(Savaco);

            // Insert accounts
            Account Jessica = new Account {Role= Administrator, Company = Savaco, Name = "Risack", Firstname = "Jessica", Email = "jessicarisack@hotmail.com", Password = "azerty123", Verified = true, Active= true, Street = "Vissersstraat 45", City = "Harelbeke", ZIP= "8530", Country = "Belgium", Telephone = "0494833582", Fax = null, Creadate = DateTime.Now , Notes = "De zus van Lorenzo" };
            Account Lorenzo = new Account {Role = Administrator, Company = Rilegal,  Name = "Risack", Firstname = "Lorenzo", Email = "lorenzorisack@hotmail.com", Password = "azerty123", Verified = true, Active = true, Street = "Teerlingstraat", City = "Aalter", Country = "Belgium",  Creadate = DateTime.Now, Notes = "De broer van Jessica" };
            context.Accounts.Add(Jessica);
            context.Accounts.Add(Lorenzo);


            // Insert orders 
            /* context.Orders.Add(new Order { Title  = "Mijn order" });*/

            // Insert keywords
            context.Keywords.Add(new Keyword { Word = "Olifant" });

            // Insert Statuses
           /* context.Statuses.Add(new Status { Description = "Accepted" });*/

            // Insert Holidays
            context.Holidays.Add(new Holiday { Description = "Allerheiligen" });

            // Insert Orderforms
            context.Orderforms.Add(new Orderform { Name = "Test" });  

            // Insert Orderstatuses
           /* context.Orderstatuses.Add(new Orderstatus { });*/

            // Insert Accountorderforms
            context.Accountorderforms.Add(new Accountorderform { });

            // Insert Templates
            context.Templates.Add(new Template { Name = "Mijn template" });

            // Insert Settings
            context.Settings.Add(new Settings { });



            
            context.SaveChanges();
        }
    }
}
