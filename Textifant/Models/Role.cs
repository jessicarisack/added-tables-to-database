﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Role
    {
        public int Id { get; set; }

        [MaxLength(50)]
        [Required]
        public string Description { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
