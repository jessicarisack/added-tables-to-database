﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Template
    {
        public int Id { get; set; }
        public Account Account { get; set; }
        public Orderform Orderform { get; set; }
        public string Name { get; set; }
        public DateTime CreaDate { get; set; }
        public string Theme { get; set; }
        public int EstWords { get; set; }
        public string PublPlace { get; set; }
        public string Resources { get; set; }
        public string TargetDesc { get; set; }
        public string Examples { get; set; }
        public Boolean Paragrpah { get; set; }
        public Boolean CallToAction { get; set; }
        public Boolean IntLink { get; set; }
        public Boolean ExLink { get; set; }
        public string ExLinkDesc { get; set; }
        public string WritingStyle { get; set; }
        public string Purpose { get; set; }
        public string ContactForm { get; set; }
        public string PersonalForm { get; set; }
        public string Other { get; set; }
        public Boolean TechSpec { get; set; }
        public Boolean NegPoints { get; set; }
        public Boolean Source { get; set; }
        public Boolean GramRev { get; set; }
        public Boolean SenCons { get; set; }
        public string FileLoc { get; set; }
        public string FileLink { get; set; }
    }
}
