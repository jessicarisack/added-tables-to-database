﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Textifant.Models
{
    public class TextifantContext : DbContext
    {
        public TextifantContext(DbContextOptions<TextifantContext> options)
           : base(options)
        { }

        public DbSet<Role> Roles { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<Orderform> Orderforms { get; set; }
        public DbSet<Orderstatus> Orderstatuses { get; set; }

        public DbSet<Settings> Settings { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Accountorderform> Accountorderforms { get; set; }
        public DbSet<Template> Templates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
                       
            // Account
            modelBuilder.Entity<Account>().HasKey(x => x.Id);

            // Role 
            modelBuilder.Entity<Role>().HasKey(x => x.Id);
            modelBuilder.Entity<Role>()
                .HasMany(c => c.Accounts)
                .WithOne(e => e.Role)
                .IsRequired(); 

            // Company
            modelBuilder.Entity<Company>().HasKey(x => x.Id);
            modelBuilder.Entity<Company>()
                .HasMany(c => c.Accounts)
                .WithOne(e => e.Company);

            modelBuilder.Entity<Order>().HasKey(x => x.Id);

            modelBuilder.Entity<Keyword>().HasKey(x => x.Id);

            modelBuilder.Entity<Holiday>().HasKey(x => x.Date);

            modelBuilder.Entity<Orderform>().HasKey(x => x.Id);

            modelBuilder.Entity<Settings>().HasKey(x => x.Id);

            modelBuilder.Entity<Status>().HasKey(x => x.Id);

            modelBuilder.Entity<Accountorderform>().HasKey(x => x.Id);

            modelBuilder.Entity<Template>().HasKey(x => x.Id);


            modelBuilder.Entity<Orderstatus>()
            .HasKey(x => new { x.OrderId, x.StatusId });

            modelBuilder.Entity<Orderstatus>()
                .HasOne(bc => bc.Order)
                .WithMany(b => b.Orderstatuses)
                .HasForeignKey(bc => bc.OrderId);

            modelBuilder.Entity<Orderstatus>()
                .HasOne(bc => bc.Status)
                .WithMany(c => c.Orderstatuses)
                .HasForeignKey(bc => bc.StatusId);
        }
    }
}
