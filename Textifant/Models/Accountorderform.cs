﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Accountorderform
    {
        public int Id { get; set; }
        public Account Account { get; set; }
        public Orderform Orderform { get; set; }
    }
}
