﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Settings
    {
        public int Id { get; set; }
        public int CustFeedback { get; set; }
        public int DocDelete { get; set; }
        public int OrderView { get; set; }
        public double Service { get; set; }
    }
}
