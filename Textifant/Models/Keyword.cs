﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Keyword
    {
        public int Id { get; set; }
        public Order Order { get; set; }
        public string Word { get; set; }
        public double Density { get; set; }
        public Boolean ConWords { get; set; }
    }
}
