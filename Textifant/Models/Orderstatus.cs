﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Orderstatus
    { 

        //public int Id { get; set; }

        // Foreign keys are primary keys
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }


        public DateTime Date { get; set; }
        public string CustFeedback { get; set; }
        public string AdminFeedback { get; set; }
        public DateTime CustFeedbackDate { get; set; }
        public DateTime AdminFeedbackDate { get; set; }

    }
}
