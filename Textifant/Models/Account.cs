﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Textifant.Models
{
    public class Account
    {
        // PK
        public int Id { get; set; }

        // FK
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }

        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        [MaxLength(50)]
        [Required]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Street { get; set; }
        [MaxLength(50)]
        public string ZIP { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        [MaxLength(25)]
        public string Telephone { get; set; }
        [MaxLength(25)]
        public string Fax { get; set; }
        [MaxLength(50)]
        [Required]
        public string Email { get; set; }
        [MaxLength(250)]
        [Required]
        public string Password { get; set; }
        [Required]
        public Boolean Verified { get; set; }
        [Required]
        public Boolean Active { get; set; }
        [Required]
        public DateTime Creadate { get; set; }
        public DateTime Obsdate { get; set; }
        [MaxLength(10000)]
        public string Notes { get; set; }
    }
}
