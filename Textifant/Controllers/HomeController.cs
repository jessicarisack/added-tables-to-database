﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Textifant.Models;
using Textifant.ViewModels;

namespace Textifant.Controllers
{
    public class HomeController : Controller
    {
        private readonly TextifantContext textifantContext;

        public HomeController(TextifantContext context)
        {
            this.textifantContext = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Test()
        {
            // Fetch the first account, fails when there are no accounts in the database
            var account = textifantContext.Accounts.First();

            // Create viewmodel
            var viewModel = new TestViewModel
            {
                Id = account.Id,
                Name = account.Name
            };

            // Return the view with our newly created viewmodel
            return View(viewModel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
